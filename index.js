// index.js
// console.log("May Node be with you, still running");

const express = require("express");
const app = express();

//app.get(endpoint, callback);

app.listen(3000, function() {
    console.log(`Listening on 3000`);
})

//example
// // We normally abbreviate `request` to `req` and `response` to `res`.
// app.get('/', function (req, res) {
//   // do something here
// })

app.get("/", function (req, res) {
  res.send("Hello World");
});